from django.urls import path
from . import views

urlpatterns = [
    path('register', views.register),
    path('contact', views.contact),
    path('login', views.login),
    path('add-company', views.add_company),
    path('invite', views.invite),
    path('send-message', views.send_message),
    path('logout', views.logout)
]