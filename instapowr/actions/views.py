from django.shortcuts import render, redirect, reverse
from django.apps import apps
from django.http import HttpResponseRedirect, HttpResponse
import random, string, smtplib, ssl
from threading import Thread


# Create your views here.
def register(request):
    if request.method == 'POST':
        user = apps.get_model('user_management', 'User')
        try:
            user.objects.get(email=request.POST.get('email'))
            data = {'s': 'Account with this email already exists'}
            return render(request, 'user_management/register.html', context=data)
        except user.DoesNotExist:
            user.objects.create(name=request.POST.get('name'), email=request.POST.get('email'), phone=request.POST.get('phone'), password=user.create_password(user, request.POST.get('password')), part_of_account=False)
        return redirect(reverse('login'))
    else:
        return redirect(reverse('index'))


def login(request):
    if request.method == 'POST':
        user = apps.get_model('user_management', 'User')
        try:
            user = user.objects.get(email=request.POST.get('email'))
        except user.DoesNotExist:
            data = {'s': 'Email or password is invalid'}
            return render(request, 'user_management/login.html', context=data)
        if not user.check_password(request.POST.get('password')):
            data = {'s': 'Email or password is invalid'}
            return render(request, 'user_management/login.html', context=data)

        token = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for x in range(64))
        user.token = token
        user.is_active = True
        user.save()
        response = HttpResponseRedirect('/dashboard')
        response.set_cookie('tokenInstapowr', token, max_age=1800)
        response.set_cookie('emailInstapowr', request.POST.get('email'), max_age=1800)
        return response


def send_email(x, name, email, subject, msg):
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("mail.instapowr.nl", 465, context=context) as server:
        if x == 1:
            server.login("messages@instapowr.nl", 'MJuYkfVb2p')
            message = f"""
            Name: {name}
            Subject: {subject}
            Message: 
            {msg}
            """
            server.sendmail(email, 'stan@instapowr.nl', message)
        elif x == 2:
            server.login("stan@instapowr.nl", 'Eenden2003@')
            message = f"""
                        Name: {name}
                        Subject: {subject}
                        Message: 
                        {msg}
                        """
            server.sendmail('stan@instapowr.nl', email, message)


def contact(request):
    if not request.POST.get('terms'):
        error = 'Please accept the terms and conditions'
        return HttpResponseRedirect(f'/contact?error={error}')
    name = request.POST.get('name')
    email = request.POST.get('email')
    subject = request.POST.get('subject')
    message = request.POST.get('message')
    mail_thread = Thread(target=send_email, args=(1, name, email, subject, message)).start()
    return HttpResponseRedirect(f'/contact?message={"Succes! You will receive a reply shortly."}')


def add_company(request):
    company = request.POST.get('company').lower()
    admin_account = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('email'))
    a = apps.get_model('user_management', 'Account').objects.create(company=company, admin_account=admin_account)
    a.accounts.add(admin_account)
    a.save()
    admin_account.part_of_account = a.pk
    admin_account.save()
    return HttpResponseRedirect('/dashboard')


def invite(request):
    company = request.POST.get('company')
    email = request.POST.get('email')
    company_email = request.POST.get('emailCompany')
    account = apps.get_model('user_management', 'Account')
    try:
        account = account.objects.get(company=company.lower())
    except account.DoesNotExist:
        return HttpResponseRedirect(f'/dashboard/add-package?msg={"Company account does not exist"}')
    user_email = account.admin_account.email
    if account.admin_account.email is not company_email:
        return HttpResponseRedirect(f'/dashboard/add-package?msg={"The email of the admin account and the provided email do not match."}')
    else:
        msg = f"""
        Hi {account.admin_account.name}, a new user with the has requested an invite for access to your account.
        
        His or her email address is: {email}.
        
        Go to: 'url' to accept the invite, or simply ignore this email.

        Regards,
        Stan
        """
        send_email(2, account.admin_account.name, company_email, f'{email} requested an invite.', msg)
        return HttpResponseRedirect(f"/dashboard/add-package?msg={'Succes. The invite has been send. We will notify you when/if the invite gets accepted'}")


def logout(request):
    response = HttpResponseRedirect('/login')
    response.delete_cookie('tokenInstapowr')
    response.delete_cookie('emailInstapowr')
    return response