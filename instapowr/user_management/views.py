from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.apps import apps

# Create your views here.
def authenticate(request):
    try:
        token = request.COOKIES.get('tokenInstapowr')
        email = request.COOKIES.get('emailInstapowr')
    except:
        return 1

    if not token or not email:
        return 2
    user = apps.get_model('user_management', 'User')
    try:
        user.objects.get(email=email, token=token)
    except user.DoesNotExist:
        return 3
    return True


def register(request):
    return render(request, 'user_management/register.html')

def login(request):
    auth = authenticate(request)
    if auth is True:
        return HttpResponseRedirect('/dashboard')
    return render(request, 'user_management/login.html')