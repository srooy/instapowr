from django.db import models
from django.contrib.auth.hashers import make_password, check_password

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=64, )
    email = models.EmailField(max_length=64)
    phone = models.CharField(max_length=16)
    password = models.CharField(max_length=64)
    token = models.CharField(max_length=64, null=True, default=None)
    part_of_account = models.IntegerField(max_length=6)

    def create_password(self, raw):
        return make_password(raw)

    def check_password(self, raw):
        return check_password(raw, self.password)

    def __str__(self):
        return self.part_of_account

class Package(models.Model):
    name = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    annual_price = models.DecimalField(max_digits=6, decimal_places=2)
    stripe_id_monthly = models.CharField(max_length=32)
    stripe_id_annual = models.CharField(max_length=32)
    free_accounts = models.IntegerField(max_length=4)
    paid_accounts = models.IntegerField(max_length=2)

    def __str__(self):
        return f"{self.name} - {self.price} - {self.annual_price}"


class IgAccount(models.Model):
    name = models.CharField(max_length=64)


class TwitterAccount(models.Model):
    name = models.CharField(max_length=64)


class FacebookAccount(models.Model):
    name = models.CharField(max_length=64)

class Account(models.Model):
    company = models.CharField(max_length=64)
    admin_account = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    package = models.ForeignKey(Package, on_delete=models.DO_NOTHING, null=True, default=None)
    auto_renew = models.BooleanField(null=True, default=None)
    ig_accounts = models.ManyToManyField(IgAccount, null=True, default=None)
    twitter_accounts = models.ManyToManyField(TwitterAccount, null=True, default=None)
    facebook_accounts = models.ManyToManyField(FacebookAccount, null=True, default=None)
    accounts = models.ManyToManyField(User, related_name='user', null=True, default=None)
    validate_until = models.DateTimeField(null=True, default=None)

