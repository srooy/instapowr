from django.apps import AppConfig
from django.contrib import admin
from .models import *


class UserManagementConfig(AppConfig):
    name = 'user_management'

admin.site.register(Package)
admin.site.register(User)
admin.site.register(Account)
