from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='dash_index'),
    path('add-package', views.add_package, name='add_package'),
    path('add-company', views.add_company, name='add_company'),
    path('package/<int:package>', views.package, name='packages')
]