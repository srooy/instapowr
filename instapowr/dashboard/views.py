from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.apps import apps
import json


# Create your views here.
def authenticate(request):
    try:
        token = request.COOKIES.get('tokenInstapowr')
        email = request.COOKIES.get('emailInstapowr')
    except:
        return HttpResponseRedirect('/login')

    if not token or not email:
        return HttpResponseRedirect('/login')
    user = apps.get_model('user_management', 'User')
    try:
        user.objects.get(email=email, token=token)
    except user.DoesNotExist:
        return HttpResponseRedirect('/login')
    return True


def check_for_company(request):
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailInstapowr'))
    if not user.part_of_account:
        return False
    return True

def check_for_package(request):
    account = apps.get_model('user_management', 'Account')
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailInstapowr'))
    if user.part_of_account is not None:
        account = account.objects.get(pk=user.part_of_account)
    else:
        return False
    if account.package is None:
        return False
    return True


def index(request):
    if authenticate(request) is not True:
        return HttpResponseRedirect('/login')
    elif check_for_company(request) is not True:
        return HttpResponseRedirect('/dashboard/add-company')
    elif check_for_package(request) is not True:
        return HttpResponseRedirect('/dashboard/add-package')
    else:
        return render(request, 'dashboard/dashboard.html')


def add_package(request):
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('email'))
    account = apps.get_model('user_management', 'Account').objects.get(accounts=user)
    if account.package is not None:
        return HttpResponseRedirect('/dashboard')
    if authenticate(request) is not True:
        return HttpResponseRedirect('/login')
    elif check_for_company(request) is not True:
        return HttpResponseRedirect('/dashboard/add-company')
    else:
        try:
            msg = request.GET.get('msg')
            return render(request, 'dashboard/add-package.html', context={'s': msg})
        except:
            return render(request, 'dashboard/add-package.html')

def add_company(request):
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('email'))
    if user.part_of_account is not None:
        return HttpResponseRedirect('/dashboard')
    if authenticate(request) is not True:
        return HttpResponseRedirect('/login')
    else:
        return render(request, 'dashboard/add-company.html')


def package(request, package):
    if authenticate(request) is not True:
        return HttpResponseRedirect('/login')
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('email'))
    account = apps.get_model('user_management', 'Account').objects.get(accounts=user)
    if account.package is not None:
        return HttpResponseRedirect('/dashboard')
    if package not in [1, 2, 3,4]:
        return HttpResponseRedirect('/dashboard')
    if package == 1:
        user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('email'))
        account = apps.get_model('user_management', 'Account').objects.get(accounts=user)
        account.package = apps.get_model('user_management', 'Package').objects.get(pk=1)
        account.save()
        return HttpResponseRedirect('/dashboard')
    elif package == 2:
        package = apps.get_model('user_management', 'Package').objects.get(pk=2)
    elif package == 3:
        package = apps.get_model('user_management', 'Package').objects.get(pk=3)
    elif package == 4:
        package = apps.get_model('user_management', 'Package').objects.get(pk=4)

    return render(request, 'dashboard/package.html', context={'package': package})

