from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, 'web/index.html')


def pricing(request):
    return render(request, 'web/pricing.html')


def contact(request):
    error = request.GET.get('error')
    message = request.GET.get('message')
    if error:
        content = {
            'error': error
        }
        return render(request, 'web/contact.html', context=content)
    elif message:
        content = {
            'message': message
        }
        return render(request, 'web/contact.html', context=content)
    else:
        return render(request, 'web/contact.html')