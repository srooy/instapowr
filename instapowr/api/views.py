from django.shortcuts import render, HttpResponseRedirect
from django.http import HttpResponseForbidden
from django.apps import apps

# Create your views here.
def authenticate(request):
    try:
        token = request.COOKIES.get('tokenInstapowr')
        email = request.COOKIES.get('emailInstapowr')
    except:
        return HttpResponseRedirect('/login')

    if not token or not email:
        return HttpResponseRedirect('/login')
    user = apps.get_model('user_management', 'User')
    try:
        user.objects.get(email=email, token=token)
    except user.DoesNotExist:
        return HttpResponseRedirect('/login')
    return True


def check_for_company(request):
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailInstapowr'))
    if not user.part_of_account:
        return False
    return True

def check_for_package(request):
    account = apps.get_model('user_management', 'Account')
    user = apps.get_model('user_management', 'User').objects.get(email=request.COOKIES.get('emailInstapowr'))
    if user.part_of_account is not None:
        account = account.objects.get(pk=user.part_of_account)
    else:
        return False
    if account.package is None:
        return False
    return True


def check_all(request):
    if request.method != 'POST':
        return HttpResponseForbidden()
    if authenticate(request) is not True:
        return HttpResponseRedirect('/login')
    elif check_for_company(request) is not True:
        return HttpResponseRedirect('/dashboard/add-company')
    elif check_for_package(request) is not True:
        return HttpResponseRedirect('/dashboard/add-package')
    else:
        return True


def messages(request):
    """Get's called from /latest-messages. This view returns the last 30 messages in chronological order.
    Structure of request:
    email: is email stored in cookies
    token: is token stored in cookies

    :param request:
    :return: latest messages || failed
    """
    check = check_all(request)
    if check is not True:
        return check

    pass


def chat_history(request):
    """Get's called from /chat-history. This view returns the complete chat history in between two accounts.
    Structure of the request:
    email: is email stored in cookies
    token: is token stored in cookies
    account: is account for which history must return
    :param request:
    :return: chat history between two accounts || failed
    """
    pass

def send_message(request):
    """ Get's called from /send-message. This view sends a message to a specified Instagram account.
    Structure of request:
    email: is email stored in cookies
    token: is token stored in cookies
    message: is message to send
    to: is account to send message to

    :param request:
    :return: succes || failed
    """
    pass